name := """perf_test"""
organization := "com.pearson"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala, JavaAgent)

scalaVersion := "2.12.10"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += "io.kamon" %% "kamon-bundle" % "2.0.4"
libraryDependencies += "io.kamon" %% "kamon-prometheus" % "2.0.1"
libraryDependencies ++= Seq(
  "org.reactivemongo" %% "play2-reactivemongo" % "0.20.3-play27",
  "com.github.stijndehaes" %% "play-prometheus-filters" % "0.5.0"
)
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.pearson.controllers._"
resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

scalacOptions in ThisBuild ++= Seq("-feature", "-language:postfixOps")

// Adds additional packages into conf/routes

// play.sbt.routes.RoutesKeys.routesImport += "com.pearson.binders._"
mainClass in Compile := Some("play.core.server.ProdServerStart")
mainClass in assembly := Some("play.core.server.ProdServerStart")
fullClasspath in assembly += Attributed.blank(PlayKeys.playPackageAssets.value)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _*) => MergeStrategy.discard
  case "reference.conf" => MergeStrategy.concat
  case _ => MergeStrategy.first
}