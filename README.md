# Configure MongoDB

Just change it in application.conf
```
mongodb.uri = "mongodb://localhost/persons"
```

# Run it
```
sbt run
```

# Persons

Person controller follows the Coast to coast Json approach.

## Add one person

```
curl -H "Content-Type: application/json" -X POST -d '{"name":"jasono","age": 12, "created": 1593421376}' http://localhost:9000/persons/
```

## Add some persons in bulk

```
curl -H "Content-Type: application/json" -X POST -d '[{"name":"jacob","age": 35, "created": 1593421376}, {"name":"rohit","age": 27, "created": 1593421376}, {"name":"federico","age": 31, "created": 1593421376}, {"name":"oliver","age": 33, "created": 1593421376}]' http://localhost/persons/bulk
```

Test invalid formats:
```
curl -H "Content-Type: application/json" -X POST -d '[{"name": "pepe"}]' http://localhost:9000/persons/bulk
```

## List first page (50 records) of all persons ordered by creation date

```
curl http://localhost:9000/persons/1
```

## Search persons by name ordered by creation date

```
curl http://localhost:9000/persons/name/jason
```

## Count all persons in collection

```
curl http://localhost:9000/persons/count
```