package filters

import javax.inject.Inject
import play.api.http.DefaultHttpFilters
import com.github.stijndehaes.playprometheusfilters.filters.StatusAndRouteLatencyAndCounterFilter

class PromFilters @Inject() (
                            latencyFilter: StatusAndRouteLatencyAndCounterFilter
                          ) extends DefaultHttpFilters(latencyFilter)