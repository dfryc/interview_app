package models

import reactivemongo.bson.{BSONLong}
import reactivemongo.play.json._


case class User(age: Int, name: String, created: BSONLong)
case class Count(usersCount: Long)

object JsonFormats{
  import play.api.libs.json._

  implicit val userFormat: OFormat[User] = Json.format[User]
  implicit val countFormat: OFormat[Count] = Json.format[Count]
}

