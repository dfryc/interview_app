package controllers

import javax.inject._
import play.api.libs.json._
import play.api.mvc._
import play.api.Logger
import play.api.libs.json.Reads._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.{Cursor, QueryOpts, ReadConcern, ReadPreference}
import reactivemongo.play.json._, collection._
import scala.concurrent.{ExecutionContext, Future}
import sys.process._
import models._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class PersonController @Inject()(components: ControllerComponents, val reactiveMongoApi: ReactiveMongoApi) extends AbstractController(components)
  with MongoController with ReactiveMongoComponents {

  implicit def ec: ExecutionContext = components.executionContext
  private val logger = Logger(this.getClass)
  val roles = List("teacher", "student", "parent")

  def personsFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("persons"))

  def createFromJson = Action.async(parse.json) { request =>
    import models.JsonFormats._
    request.body.validate[User].map { result =>
      personsFuture.flatMap(_.insert.one[User](result)).map { lastError =>
        logger.debug(s"Successfully inserted with LastError: $lastError")
        Created
      }
    }.getOrElse(Future.successful(BadRequest("invalid json")))
  }


  def createBulkFromJson = Action.async(parse.json) { request =>
    //Transformation silent in case of failures.
    import models.JsonFormats._
    request.body.validate[List[User]].map {users: Seq[User] =>
      personsFuture.flatMap(_.insert.many(users)).map{lastError =>
        logger.debug(s"Successfully inserted with LastError: $lastError")
        Created
      }
    }.getOrElse(Future.successful(BadRequest("invalid JSON")))
  }

  def findAll(page: Int = 1) = Action.async {
    import models.JsonFormats._
    val maxDocs: Int = 50
    // let's do our query

    val cursor: Future[Cursor[User]] = personsFuture.map {
      // find all people
      _.find(Json.obj()).
        options(QueryOpts((page-1)*maxDocs, maxDocs)).maxTimeMs(10000).
        // sort them by creation date
        sort(Json.obj("created" -> -1)).
        // perform the query and get a cursor of JsObject
        cursor[User]()
    }
    // gather all the JsObjects in a list
    val futureUsersList: Future[List[User]] =
      cursor.flatMap(_.collect[List](maxDocs, Cursor.FailOnError[List[User]]()))


    // everything's ok! Let's reply with a JsValue
    futureUsersList.map { persons =>
      Ok(Json.toJson(persons))
    }
  }

  def findByName(name: String) = Action.async {
    import models.JsonFormats._
    // let's do our query
    val cursor: Future[Cursor[User]] = personsFuture.map {
      // find all people with name `name`
      _.find(Json.obj("name" -> name)).
        // sort them by creation date
        sort(Json.obj("created" -> -1)).
        // perform the query and get a cursor of JsObject
        cursor[User]()
    }

    // gather all the JsObjects in a list
    val futureUsersList: Future[List[User]] =
      cursor.flatMap(_.collect[List](-1, Cursor.FailOnError[List[User]]()))

    // everything's ok! Let's reply with the array
    futureUsersList.map { persons =>
      Ok(Json.toJson(persons))
    }
  }

  def countAll = Action.async {
    import models.JsonFormats.countFormat
    for {
      persons <- personsFuture
      result <- persons.count(None, limit = None, skip = 0, hint = None, readConcern = ReadConcern.Available)
    } yield {
      Ok(Json.toJson(Count(result)))
    }
  }
}
